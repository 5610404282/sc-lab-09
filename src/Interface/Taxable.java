package Interface;

import Model.Person;

public interface Taxable {
	
	public double getTax();
	
}
