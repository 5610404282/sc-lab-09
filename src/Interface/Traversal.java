package Interface;

import java.util.List;
import Model2.Node;

public interface Traversal {
	List<String> traverse(Node node);
}
