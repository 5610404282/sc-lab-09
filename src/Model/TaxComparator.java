package Model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements  Comparator {
	
	@Override
	public int compare(Object o1, Object o2) {
		double tax = ((Taxable)o1).getTax();
		double tax2 = ((Taxable)o2).getTax();
		if (tax > tax2) return 1;
		if (tax < tax2) return -1;
		return 0;
		
	}

}
