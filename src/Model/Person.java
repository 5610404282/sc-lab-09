package Model;

import java.util.Comparator;

import Interface.Measurable;
import Interface.Taxable;

public class Person implements Measurable,Taxable,Comparable<Person>{
	private String name;
	private int height;
	private double yearlyIncome;

	public Person(String name, int height) {
		this.name = name;
		this.height = height;
	}

	public Person(String name, int height, double yearlyIncome) {
		this(name, height);
		this.yearlyIncome = yearlyIncome;
	}

	public String getName() {
		return this.name;
	}

	public int getHeight() {
		return this.height;
	}

	public double getYearlyIncome() {
		return this.yearlyIncome;
	}

	@Override
	public double getMeasure() {
		return this.getHeight();
	}

	@Override
	public int compareTo(Person other) {
		if(this.getYearlyIncome() < other.getYearlyIncome())
			return -1;
		else if (this.getYearlyIncome() > other.getYearlyIncome())
			return 1;
		else
			return 0;

	}
	
	@Override
	public double getTax() {
		double income = this.getYearlyIncome();
		double result = 0;

		if (income > 300000) {
			result += 0.05 * 300000;
			income -= 300000;
			result += 0.10 * income;
		}else{
			result += 0.05 * income;
		}
		return result;
	}

	
	public String toString(){
		return "name = "+name +", Yearly income = "+yearlyIncome + ", tax = "+getTax();
	}


}
