package Company;

import Model.Company;

import java.util.Comparator;

public class EarningComparator implements Comparator {
	
	@Override
	public int compare(Object o1, Object o2) {
		double income = ((Company)o1).getIncome();
		double income2 = ((Company)o2).getIncome();
		if (income > income2) return 1;
		if (income < income2) return -1;
		return 0;
		
	}

}
