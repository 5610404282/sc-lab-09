package Company;

import java.util.Comparator;

import Model.Company;

public class ExpenseComparator implements Comparator {
	
	@Override
	public int compare(Object o1, Object o2) {
		double expenses = ((Company)o1).getExpenses();
		double expenses2 = ((Company)o2).getExpenses();
		if (expenses > expenses2) return 1;
		if (expenses < expenses2) return -1;
		return 0;
		
	}

}
