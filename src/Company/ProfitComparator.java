package Company;

import java.util.Comparator;

import Model.Company;

public class ProfitComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		double profit = ((Company)o1).getProfit();
		double profit2 = ((Company)o2).getProfit();
		if (profit > profit2) return 1;
		if (profit < profit2) return -1;
		return 0;
		
		
	}

}
