package Controller;

import java.util.ArrayList;
import java.util.List;

import Model.Person;
import Model2.InOrderTraversal;
import Model2.Node;
import Model2.PostOrderTraversal;
import Model2.PreOrderTraversal;
import Model2.ReportConsole;

public class TraverseTest {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TraverseTest  tester = new TraverseTest ();
		tester.testTraverse();
	}

	private void testTraverse() {
		
		Node nine = new Node("H",null,null);
		Node eight = new Node("E",null,null);
		Node seven = new Node("C",null,null);
		Node six = new Node("I",nine,null);
		Node five = new Node("D",seven,eight);
		Node four = new Node("A",null,null);
		Node three = new Node("G",null,six);
		Node two = new Node("B",four,five);
		Node one = new Node ("F",two,three);
		
		PreOrderTraversal pre = new PreOrderTraversal();
		pre.traverse(one);
		ReportConsole.display(one,pre);
		
		InOrderTraversal in = new InOrderTraversal();
		in.traverse(one);
		ReportConsole.display(one,in);
		
		PostOrderTraversal post = new PostOrderTraversal();
		in.traverse(one);
		ReportConsole.display(one,post);
		
	}

}
