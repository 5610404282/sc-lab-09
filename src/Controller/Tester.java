package Controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Company.EarningComparator;
import Company.ExpenseComparator;
import Company.ProfitComparator;
import Model.Company;
import Model.Person;
import Model.Product;
import Model.TaxComparator;
import Interface.Taxable;

public class Tester {
	
	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.testQuestion1a();

	}

	
	public void testQuestion1a(){
		List<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Luhan", 175, 150000));
		persons.add(new Person("Jinhwan", 165, 7000000));
		persons.add(new Person("Chanyeol", 182, 28000));
		System.out.println("------ Before sort with Yearly income ------");
		
		for (Person c : persons) {
			System.out.println(c);
		}
		
		Collections.sort(persons);
		System.out.println("------ After sort with Yearly income -------");
		for (Person c : persons) {
			System.out.println(c);
		}
		
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("Computer", 350000));
		products.add(new Product("Poster",100));
		products.add(new Product("Bike",2300));
		
		System.out.println("\n"+"------ Before sort with Price ------");
		for (Product c : products) {
			System.out.println(c);
		}
		Collections.sort(products);
		System.out.println("------ After sort with Price -------");
		for (Product c : products) {
			System.out.println(c);
		}
		
		List<Company> companys = new ArrayList<Company>();
		companys.add(new Company("SM", 560000, 70000));
		companys.add(new Company("JYP", 200000, 4500));
		companys.add(new Company("YG", 68000, 5300));
		
		System.out.println("\n"+"------ Before sort  ------");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		Collections.sort(companys, new EarningComparator());
		System.out.println("------ After sort with income -------");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		Collections.sort(companys, new ExpenseComparator());
		System.out.println("------ After sort with expense -------");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		Collections.sort(companys, new ProfitComparator());
		System.out.println("------ After sort with profit -------");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		List<Taxable> taxperson = new ArrayList<Taxable>();
		ArrayList<Taxable> taxcompany = new ArrayList<Taxable>();
		ArrayList<Taxable> taxproduct = new ArrayList<Taxable>();
		ArrayList<Taxable> taxsum = new ArrayList<Taxable>();
		
		taxperson.add(new Person("Luhan",175,500000));
		taxperson.add(new Person("Jinhwan",165,245000));
		System.out.println("\n------ Before sort with tax ------");
		
		for (Taxable c : taxperson ) {
			System.out.println(c);
		}
		
		Collections.sort(taxperson, new TaxComparator());
		System.out.println("------ After sort with tax -------");
		for (Taxable c : taxperson) {
			System.out.println(c);
		}
		
		taxproduct.add(new Product("Book",200));
		taxproduct.add(new Product("Poster",100));
		taxproduct.add(new Product("Bike",2300));
		
		System.out.println("\n------ Before sort with tax ------");
		
		for (Taxable c : taxproduct) {
			System.out.println(c);
		}
		
		Collections.sort(taxproduct, new TaxComparator());
		System.out.println("------ After sort with tax -------");
		for (Taxable c : taxproduct) {
			System.out.println(c);
		}
		
		
		taxcompany.add(new Company("SM",1580000,400000));
		taxcompany.add(new Company("JYP", 200000, 4500));
		taxcompany.add(new Company("YG", 680000, 530000));
		
		System.out.println("\n------ Before sort with tax ------");
		
		for (Taxable c : taxcompany) {
			System.out.println(c);
		}
		
		Collections.sort(taxcompany, new TaxComparator());
		System.out.println("------ After sort with tax -------");
		for (Taxable c : taxcompany) {
			System.out.println(c);
		}
		
		
		taxsum.add(new Person("Luhan",175,500000));
		taxsum.add(new Person("Jinhwan",165,245000));
		taxsum.add(new Company("SM",1580000,400000));
		taxsum.add(new Product("Book",200));
		
		System.out.println("\n------ Before sort with tax ------");
		for (Taxable c : taxsum) {
			System.out.println(c);
		}
		
		Collections.sort(taxsum, new TaxComparator());
		System.out.println("------ After sort with tax -------");
		for (Taxable c : taxsum) {
			System.out.println(c);
		}
	}

}
