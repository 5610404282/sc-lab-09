package Model2;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import Interface.Traversal;

public class InOrderTraversal implements Traversal {
	
	
	@Override
	public List<String> traverse(Node node) {
		List<String> list = new ArrayList<String>();
		 
        if(node == null)
            return list; 
 
        Stack<Node> stack = new Stack<Node>();
        Node p = node;
 
        while(!stack.empty() || p != null){
            if(p != null){
                stack.push(p);
                p = p.getleft();
            }
            else{
                Node t = stack.pop();
                list.add(t.getvalue());
                p = t.getright();
            }
        }
 
        return list;
	}

}
