package Model2;

public class Node {
	
	private String value;
	private Node left;
	private Node right;
	
	public Node(String value,Node left,Node right){
		this.value = value;
		this.left = left;
		this.right = right;
	}
	
	public String getvalue(){
		return this.value;
	}
	
	public Node getleft(){
		return this.left;
	}
	
	public Node getright(){
		return this.right;
	}
}
