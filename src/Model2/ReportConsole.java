package Model2;

import java.util.List;

import Interface.Traversal;
import Model.Person;
import Model.Product;

public class ReportConsole {
	public static void display(Node node,Traversal t){
		List<String> ans = null;
		if(t instanceof PreOrderTraversal ){
			ans = t.traverse(node);
			System.out.print("Traverse with PreOrderTraversal : ");
			for (String c : ans) {
				System.out.print(c+" ");
			}
			System.out.println("");
		}
		
		else if(t instanceof InOrderTraversal ){
			ans = t.traverse(node);
			System.out.print("Traverse with InOrderTraversal : ");
			for (String c : ans) {
				System.out.print(c+" ");
			}
			System.out.println("");
		}
		else{
			ans = t.traverse(node);
			System.out.print("Traverse with PostOrderTraversal : ");
			for (String c : ans) {
				System.out.print(c+" ");
			}
			System.out.println("");
		}
		
	}
}
